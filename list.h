#pragma once
#ifndef LIST
#define LIST
#include<iostream>
using namespace std;

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
class Node {
public:
	T info;
	Node<T> *next, *prev;
	Node() { next = prev = 0; }
	Node(T el, Node *n = 0, Node *p = 0) { info = el; next = n; prev = p; }
};
template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
class List {
public:
	List() { head = tail = 0; }
	~List();
	int isEmpty() { return head == 0; } //function for the empty list
	void headPush(T);//function for pushing any kind of data into the front the list
	void tailPop(); //function for pop out any kind of data in the tail of the list
	void swap(Node<T>*, Node<T>*); //function for swapping any kind of data in the list(2 nodes)
	void sort(); //function for sorting the list
	Node<T>* smallest_data(Node<T>*); //function for finding the node which has the lowest value
	void Unique(); //function for removing duplicates
	void display(); //function for showing the list


private:
	Node<T> * head, *tail;
};
template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
List<T>::~List()
{
	for (Node<T> *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
void List<T>::headPush(T data)
{
	Node<T> *tmp = new Node<T>(data); //create a new node which contained the value that we are going to put it in the front of the list
	if (isEmpty()) { //condition for the empty list
		head = tmp; //make pointer head point to tmp
		tail = tmp; //make pointer tail point to tmp
	}
	else {
		head->prev = tmp; //make the node that is before head pointer become tmp
		tmp->next = head; //make the node that is next to tmp pointer become head
		head = tmp; //make pointer head point to tmp
	}
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
void List<T>::tailPop()
{
	T x = 0; //create an integer that will be used to collect and return the element that will be popped out
	Node<T> *tmp = tail; //create node that will replace the tail pointer position in order to help us be able to delete that node
	if (isEmpty()) { //condition for the empty list

	}
	else if (head == tail) { //condition when there is just one element in the list
		head = NULL; //make pointer head point to NULL
		tail = NULL; //make pointer tail point to NULL
		x = tmp->info; //make x equal to the value where pointer tmp point to
		delete tmp; //delete pointer tmp;
	}
	else {
		tail = tail->prev; //make tail pointer move backward 1 node
		delete tmp; //delete pointer tmp;
		tail->next = NULL; //make the node that is next to head equal to NULL
	}
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
void List<T>::swap(Node<T>* variable1, Node<T>* variable2) //declare 2 nodes that are going to contain 2 data
{
	T temp; //declare a variable which can contain any kind of data
	temp = variable1->info; //make the variable that I created equal to the data in Node named variable1
	variable1->info = variable2->info; //then change the data in Node named variable1 by making it equal to data in Node named variable2
	variable2->info = temp;
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
void List<T>::sort() {
	Node<T> *tmp = head; //declare a Node named tmp and make it point to where the head pointer is
	Node<T> *newtmp; //declare a Node named newtmp;
	if (isEmpty()) { //condition for the empty list
		return;
	}
	while (tmp->next != NULL) //loop for making the checking and swapping algorithm work until the list is sorted
	{
		newtmp = smallest_data(tmp); //Node newtmp will contain the smallest value which is the result from smallest_data function
		swap(tmp, newtmp);  //use the swap function to switch the data between 2 nodes
		tmp = tmp->next; //make tmp pointer move forward 1 node
	}
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
Node<T>* List<T>::smallest_data(Node<T>*a)
{
	Node<T>* smallest; //declare a node named smallest
	smallest = a; //make node smallest point to the same point as node a
	while (a != NULL) //loop that let the finding the smallest algorithm works until we find the node that contain the smallest value between each node
	{
		if (smallest->info > a->info) //condition for comaparing data in 2 nodes
		{
			smallest = a; //condition if the value in the node where pointer a point to is smaller than the value contain in node smallest 
		}
		a = a->next; //move pointer a forward 1 node
	}
	return smallest; //return pointer smallest
}

template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
inline void List<T>::Unique()
{
	if (isEmpty() || head->next == NULL) {  //condition for the empty list and if there is just only 1 elements in the list
		return;
	}
	Node<T> *tmp = head; //create Node named tmp and make it point to the head

	T h = 0; //create a variable to represent data in the head
	T t = 0; //create a variable to represent data in the tail
	h = head->info; //make variable h = data inside the node where head pointer point to
	t = tail->info; //make variable t = data inside the node where tail pointer point to
	while (h == t) { //loop that will work as long as the data in the node that head pointer and tail pointer point to are the same
		if (head == tail) { //if the algorithm work and delete the node until only 1 element left stop tailPop function
			return;
		}
		tailPop(); //use tailPop function to pop off the last element from the list
		h = head->info; //make variable h = data inside the node where head pointer point to
		t = tail->info; //make variable t = data inside the node where tail pointer point to
	}

	while (tmp->next != tail) { //loop that enable the checking duplicate algorithm work until the node before tail
		Node<T> *keep = tmp->next; //daclare a node named keep and make it point to the node next to where pointer is pointing to
		while (keep != tail) { //loop that make the algorithm work until keep is moved and reach the node that is the previous of tail
			if (keep->info == tmp->info) { //condition when the data in 2 nodes are the same
				Node<T> *del = keep; //create node named del and make it point to where pointer keep point to
				Node<T> *pre = keep->prev; //create node pre and make the node that is next to it becomes keep
				keep = keep->next; //move the node keep forward 1 node
				delete del; //delete node del
				keep->prev = pre; //make the node that is previous of keep become pre
				pre->next = keep; //make the node that is next to pre become keep
			}
			else { //condition when the data in 2 nodes are the difference
				keep = keep->next; //move the node keep forward 1 node
			}

		}
		if (keep == tail) { //condition when the node keep reach the tail
			if (keep->info == tmp->info) { //condition in case that the data in 2 nodes are the same
				tmp = tmp->prev; //move tmp forward 1 node
				tailPop(); //use function tail pop which is pop out 1 node from the back of the list
			}
		}
		if (tmp->next != tail)tmp = tmp->next; //codition for moving tmp forward in case that the node that is next to tmp is not tail yet
	}
	if (tmp->next == tail) { //condition for checking the node that is before tail and tail
		if (tail->info == tmp->info) { //condition in case that the data in 2 nodes are the same
			tailPop(); //use function tail pop which is pop out 1 node from the back of the list
		}
	}
}


template <class T> //use template in order that the nodes or variables are able to contain or represent any kind of data.
void List<T>::display()
{
	Node<T> *tmp = head; //create a node named tmp and make it points to head
	if (tmp != NULL) { //condition to display the list if it is not empty
		cout << tmp->info; //show the value where the pointer tmp point to
		while (tmp->next != NULL) { //loop for the condition if there are still node that are not equal to NULL next to tmp
			tmp = tmp->next; //move the pointer tmp forward one node
			cout << tmp->info; //show the value where the pointer tmp point to
		}
		return;
	}
	else { //condition for the empty list
		return;
	}
}

#endif //list