#include <iostream>
#include "list.h"

using namespace std;

void main()
{
	List<int> mylist;

	mylist.headPush(8);
	mylist.headPush(8);
	mylist.headPush(8);
	mylist.headPush(7);
	mylist.headPush(6);
	mylist.headPush(5);
	mylist.headPush(4);
	mylist.headPush(8);
	mylist.headPush(6);
	mylist.headPush(4);
	mylist.headPush(8);


	mylist.Unique();

	mylist.sort();
	mylist.display();

	cout << endl;

	List<char> mylist2;

	mylist2.headPush('h');
	mylist2.headPush('e');
	mylist2.headPush('a');
	mylist2.headPush('e');
	mylist2.headPush('c');
	mylist2.headPush('d');
	mylist2.Unique();

	mylist2.sort();
	mylist2.display();

	cout << endl;
	
	system("pause");
	//return 0;
}